---
title: Marketing Plan PayTren
description: marketing plan PayTren
slug: marketing-plan

---
Bussiness Plan/Marketing Plan Paytren merupakan salah satu Bonus Plan paling Revolusioner dalam Industri Network Marketing, dimana semua system didalamnya sangat sesuai dengan hukum pemerintah yg berlaku dan juga peraturan dari APLI, tidak sampai disitu saja, Bonus Plan Paytren juga sangat sesuai dengan Hukum Syari’ah Islam, terbukti dengan adanya DSN sendiri yg mengawasi dan meninjau bonus plan nya. Sehingga inilah yang merupakan salah satu kunci sukses [PAYTEN](http://paytren.online/) bisa menembus semua kalangan.  

### **6 Sumber Income Anda**

1. **Bonus Sponsor**

**2. Bonus Leadership**

**3. Bonus Generasi Sponsor**

**4. Bonus Matching Leadership**

**5. Bonus Cash Back Transaksi**

**6. Bonus Reward**

### **1. KOMISI PENJUALAN REFFERAL ( BONUS SPONSOR )**

Perusahaan akan memberikan komisi penjualan langsung kepada mitra pebisnis (ANDA) yang berhasil menjual langsung paket lisensi **PayTren** sebesar Rp. 75.000,00 per HU (hak usaha). 

* Tidak ada batasan jumlah sponsoring, sehingga potensinya tidak terbatas
* Semua Mitra yang Anda sponsor disebut generasi 1 Anda

![](/images/Bonus-Sponsor.png)

### **2. KOMISI LEADERSHIP (BONUS PASANGAN)**

Adalah bonus yang didapat dari perusahaan jika grup anda saling berpasangan, antara kiri dan kanan anda, tanpa melihat bentuk jaringan, maks Rp 300.000,-\* per hari (12 pasang)

![](/images/Bonus-Pasangan.png)

### **3. BONUS GENERASI SPONSOR**

Perusahaan akan memberikan komisi pengembangan penjualan sebesar Rp 2.000,00 per lisensi, apabila mitra pebisnis yang ANDA refferensikan berhasil menjual paket **PayTren** (berapapun lisensinya).

Atau Setiap orang yang Anda sponsori dan teamnya hingga Generasi 10 mensponsori mitra baru, Maka Anda berhak mendapatkan Rp.2000,-

![](/images/bonus-generasi-pasangan.png)

### **4. BONUS MATCHING LEADERSHIP**

Perusahaan memberikan komisi pengembangan komunitas sebesar Rp 1.000,- untuk setiap Mitra Pebisnis yang ANDA refferensikan (maksimal 10 turunan/generasi) berhasil mendapatkan komisi leadership.

Ketika orang-orang yang Anda sponsori dan teamnya mendapat B.Leadership, Anda berhak mendapatkan Rp.1000,- (setiap orang berpotensi mendapatkan Bonus Leadership 12 pasang perhari).

![](/images/bonus-generasi-leadership.png)

### **5. BONUS CASHBACK TRANSAKSI**

Perusahaan membagikan _cashback_ dari setiap transaksi pribadi dan transaksi komunitas Anda dengan besaran dalam bentuk prosentase dari fee yang diperoleh dari setiap biller/merchant/bank yang bekerjasama dengan perusahaan dengan syarat dimana Anda wajib melakukan transaksi minimal 1x trx/bulan

Yang dimaksud dengan komunitas Anda disini adalah mitra pengguna dan mitra pebisnis yang Anda refferensikan hingga maksimal 10 turunan/generasi dengan sistim pass up/compress (contoh: jika ada turunan ke 3 tidak melakukan tranksaksi maka turunan ke 4 akan dihitung sebagai turunan ke 3, dst hingga maksimal 10 turunan).

Cashback dihitung perhari dari tanggal 1 sampai tanggal 30/31 dan dibayarkan tanggal 15 setiap bulannya.

![](/images/bonus-cashback-transaksi.png)

### **6. BONUS REWARD**

Reward ini berlaku bagi mitra **PayTren** dimana 2 (dua) group/komunitas pebisnis yang terbentuk langsung dalam struktur organisasi/jaringan ANDA (grup kiri dan grup kanan) masing masing mencapai target omset yang ditentukan perusahaan.  