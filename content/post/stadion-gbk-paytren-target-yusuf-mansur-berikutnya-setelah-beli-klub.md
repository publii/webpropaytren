+++
categories = ["news"]
date = "2018-03-30T13:11:11+07:00"
description = "Ustaz Yusuf Mansur mempunyai hasrat lain usai mengakusisi beberapa klub Liga Indonesia. Dia ingin membeli nama Stadion Utama Gelora Bung Karno."
featured = []
thumbnail = "/images/gbk.png"
slug = ""
tags = ["paytren"]
title = "Stadion GBK Paytren, Target Yusuf Mansur Berikutnya Setelah Beli Klub"

+++
Ustaz Yusuf Mansur mempunyai hasrat lain usai mengakusisi beberapa klub Liga Indonesia. Dia ingin membeli nama Stadion Utama Gelora Bung Karno.  
  
Beberapa klub Liga 2 dan Liga 3 merupakan klub yang sudah di kelola Yusuf Mansur. Persikota Tangerang, Persika Karawang, dan Malang United merupakan deretan klub-klub itu.  
  
Kini, Yusuf Mansur mempunyai ambisi lain. Nama perusahaannya, PayTrend, ingin disematkan ke nama Stadion Gelora Bung Karno.  
  
Di manca negara, nama stadion memang biasa dijual sebagai cara untuk mendapatkan dana segar. Sebagai contoh adalah Allianz Arena milik Bayern Munich, juga Allianz Stadium milik Juventus.  
  
Selain itu, Emirartes Stadium milik Arsenal dan Etihad Stadium kepunyaan Manchester City menjadi contoh lain menganenai penjualan dari nama stadion.

| --- |
| Baca juga: Lomba Logo Malang United Berhadiah Rp 1 Juta Ramai Dibahas Netizen |

  
Rencana Yusuf Mansur untuk membeli nama SUGBK itu diungkapkan saat bertandang ke kantor detikcom, Selasa (27/2/2018). Negosiasi untuk mewujudkannya juga sudah dilakukan. Lewat PayTrend, dia siap untuk mendukung pemerintah.  
  
"Saya berterima kasih sekali pemerintah terbuka untuk siapa saja pengusaha Indonesia nge-branding stadionnya sendiri. Itu memang yang terbaik daripada stadion di Indonesia tapi yang mem-branding adalah perusahaan luar," kata Yusuf Mansur saat berbincang dengan detikcom.  
  
"Kita juga bukan cuma butuh duitnya, tapi pride and proud, lokal Indonesia, tak mesti PayTrend, tapi tiba-tiba muncul pengusaha lain mem-branding GBK. Sudah ada pembicaraan soal hal itu, kami siap mendukung untuk mendukung pemerintah," dia menambahkan.  

sumber: [https://sport.detik.com/sepakbola/liga-indonesia/d-3889186/stadion-gbk-paytren-target-yusuf-mansur-berikutnya-setelah-beli-klub?_ga=2.97348829.334846425.1522389801-1673377053.1522389796](https://sport.detik.com/sepakbola/liga-indonesia/d-3889186/stadion-gbk-paytren-target-yusuf-mansur-berikutnya-setelah-beli-klub?_ga=2.97348829.334846425.1522389801-1673377053.1522389796 "https://sport.detik.com/sepakbola/liga-indonesia/d-3889186/stadion-gbk-paytren-target-yusuf-mansur-berikutnya-setelah-beli-klub?_ga=2.97348829.334846425.1522389801-1673377053.1522389796")