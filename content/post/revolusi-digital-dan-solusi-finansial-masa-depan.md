+++
categories = ["news"]
date = "2018-03-30T10:27:34+07:00"
description = "Konferensi Blockchain Technology yang sudah banyak diselenggarakan di berbagai negara di dunia, pertama kali digelar di Indonesia"
featured = []
thumbnail = "/images/IMG-20180223-WA0044-768x576.jpg"
slug = ""
tags = ["finansial"]
title = "Revolusi Digital dan Solusi Finansial Masa Depan"

+++
Konferensi _Blockchain Technology_ yang sudah banyak diselenggarakan di berbagai negara di dunia, pertama kali digelar di Indonesia, tepatnya di The Ritz-Carlton Jakarta pada 23 Februari 2018, dengan mengangkat tema _Blockchain Technology_ dan tagline “_Meet, Share, Explore & Network_“.

Konferensi ini membahas berbagai kemungkinan dalam perkembangan _Blockchain Technology_ di dunia dan Indonesia khususnya. 

Event spesial ini menampilkan para pembicara ternama dari praktisi dan profesional dari perusahaan digital Asia antara lain, Boye Hartmann, CEO dan Founder Y Digital Group Asia PTE LTD,  Imanzah Nurhidayat, CIO CoreChain,  Bari Arijono, CEO & Founder DEI (Digital Enterprise Indonesia) , Pandu Sastrowardoyo, Chairperson, Co-Founder Blockchain Zoo,  Valentine Gandhi, Founder The Development Cafe,   Dr Achmad Istamar, CEO Esri Indonesia dan Hari Prabowo, S.E, CEO dan Co-Founder PayTren.

![](https://news.treni.co.id/wp-content/uploads/2018/02/IMG-20180223-WA0044-768x576.jpg =768x576)

Peserta konferensi ini merupakan CEO dan pimpinan perusahaan baik di bidang perbankan, asuransi,_financial technology_, perhotelan, konsultan keuangan, perusahaan IT, perkebunan, dan lainnya.  Mereka begitu antusias mengikuti seluruh tahapan presentasi dan diskusi dalam konferensi ini.  

Para pembicara sepakat bahwa Teknologi_Blockchain_adalah sebuah fenomena revolusioner di bidang teknologi digital;  semacam mekanisme yang menggiring semua orang ke tingkat akuntabilitas paling tinggi, dimana tidak ada lagi transaksi yang terlewatkan, _human error_ bahkan tidak ada lagi prosedur transaksi yang rumit. Semua serba cepat dan akurat.

Dalam kesempatan presentasinya, Bari Arijono memaparkan fakta yang terjadi di Indonesia dimana pembayaran elektronik semakin dirasakan manfaatnya oleh masyarakat seperti _E-Toll_, PayTren dan Go-Pay.  

Untuk masa yang akan datang,  transaksi seperti ini tidak lagi membutuhkan _middleman_ atau perantara seperti bank, _visa_ dan _master,_ karena sudah digantikan oleh  teknologi digital baru bernama _Blockchain._

Sementara itu Hari Prabowo, S.E CEO dan Co-Founder PayTren lewat presentasinya yang berjudul “_Changing Mindset & Culture for Blockchain Adoption_”, menjelaskan bagaimana digitalisasi telah merevolusi seluruh sendi kehidupan, baik di bidang non-keuangan maupun keuangan yang dipelopori oleh teknologi finansial. 

Penerapan teknologi _blockchain_, dalam prakteknya, dapat membuat transaksi lebih cepat, _real time_ dan efisien. _Blockchain_ juga tidak mengharuskan kehadiran fisik dan tidak memerlukan pihak ketiga, menjamin transparansi dan keamanan, serta tidak mudah mengubah data (_immutability_). 

Di akhir presentasinya, Hari Prabowo menawarkan kepada para peserta seminar untuk bersinergi dengan PayTren, terkait teknologi _Blockchain_, dalam rangka mengembangkan _platform_ bisnis _financial technology_.

sumber: [https://news.treni.co.id/revolusi-digital-solusi-finansial-masa-depan/](https://news.treni.co.id/revolusi-digital-solusi-finansial-masa-depan/ "https://news.treni.co.id/revolusi-digital-solusi-finansial-masa-depan/")