+++
categories = ["news"]
date = "2018-03-30T10:19:14+07:00"
description = "Bank Muamalat Indonesia merangkul jama’ah Ustadz Yusuf Mansur.  Rabu, 28 Februari 2018, Yusuf Mansur bersama ribuan jama’ahnya mendatangi kantor pusat Bank Muamalat Indonesia (BMI) di Muamalat Tower."
featured = []
thumbnail = "/images/muamalat.png"
slug = ""
tags = ["muamalat"]
title = "Umat Islam Mendukung Muamalat dan Industri Syariah"

+++
Bank Muamalat Indonesia merangkul jama’ah Ustadz Yusuf Mansur.  Rabu, 28 Februari 2018, Yusuf Mansur bersama ribuan jama’ahnya mendatangi kantor pusat Bank Muamalat Indonesia (BMI) di Muamalat Tower, jalan Prof. Dr. Satrio kav 18, Kuningan, Jakarta untuk bersama-sama membuka rekening. 

Pembukaan tabungan ini menurut Yusuf Mansur adalah merupakan jalan awal bagi masyarakat yang ingin agar industri perbankan syariah di Indonesia menjadi kuat.

“Insyaa Allah, umat bercita-cita mendorong industri perbankan syariah, maka inilah langkah konkrit kami membantu menanam kebaikan di Bank Muamalat Indonesia untuk membantu meningkatkan pertumbuhan ekonomi syariah secara umum. Selain bermanfaat untuk kebaikan ekonomi umat juga Insyaa Allah, kebaikan ini akan berpulang lagi ke kita, kata Yusuf Mansur.

Niat ini disambut baik oleh Direktur Utama Bank Muamalat Indonesia, Achmad K. Permana. Menurut Permana, inisiatif ini menunjukkan semakin dekatnya dukungan umat yang kuat pada Bank Muamalat dan keinginan dari umat untuk berpartisipasi pada  pertumbuhan industri perbankan syariah Indonesia. 

“Kami berterima kasih untuk hal ini, dan bank Muamalat berkomitmen tetap melayani berbagai lapisan umat sebagai bank Syariah terdepan di Indonesia”, dan bulan ini kami canangkan sebagai bulan kebersamaan Muamalat dengan umat, termasuk Paytren dan Darul Quran khususnya ungkap Permana.

Hal ini sesuai dengan strategi ke depan Bank Muamalat untuk lebih fokus kepada komunitas Islam dan hal ini juga sejalan dengan  amanah kerjasama yang telah kita bangun kembali bersama berbagai Ormas Islam diantaranya NU, Hidayatullah, Muhammadiyah dan juga  menggandeng ulama dan ustadz seperti KH. AA Gym, Ustadz Yusuf Mansur, Ustadz Arifin Ilham dan Ustadz Subhan Bawazier, dll. Insya Allah pola kerjasama seperti ini akan terus dibangun dan dikembangkan dengan berbagai komunitas Islam lainnya.

Asosiasi Bank Syariah Indonesia (ASBISINDO) melihat peluang peningkatan pangsa pasar perbankan syariah hingga 7 persen pada 2018. Sehingga bank syariah seharusnya bisa tumbuh hingga 20%, mengingat Indonesia memiliki populasi Muslim terbesar di dunia, yang mewakili 10,7 persen populasi Muslim di dunia.

Acara yang berlangsung siang tadi diisi dengan diawali dengan sholat berjamah, tausiyah dari Ustadz Yusuf Mansur dan pembukaan rekening Bank Muamalat yang disaksikan bersama KH Ma’ruf Amin selaku Ketua MUI dan Ketua Dewan Pengawas Syariah Bank Muamalat serta beberapa tokoh ekonomi syariah dan dilanjutkan ramah-tamah di masjid Muamalat di lantai 20 Muamalat Tower.

**Tentang PT Bank Muamalat Indonesia Indonesia, Tbk**

Bank Muamalat Indonesia, Tbk merupakan pionir perbankan syariah. Didirikan pada 1 November 1991 dan memulai operasi pada 1 Mei 1992 dan hingga kini Bank Muamalat Indonesia terus mempertahankan eksistensinya. Sejak berdiri, Bank Muamalat Indonesia terus berinovasi dengan meluncurkan produk-produk unggulan. 

Kartu Shar-E Gold Debit Bank Muamalat menjadi kartu _chip_pertama yang dapat digunakan untuk bertransaksi bebas biaya di jutaan _merchant_ di seluruh dunia. Kartu Shar-E Gold Debit bahkan meraih predikat sebagai Kartu Debit Syariah Berteknologi Chip Pertama di Indonesia oleh Museum Rekor Indonesia (MURI). 

Kartu Debit Arsenal yang desain barunya diluncurkan pada awal Agustus 2017 juga telah menerapkan teknologi chip standar nasional (NSICCS) untuk keamanan bertransaksi nasabah sesuai dengan ketentuan dari Bank Indonesia.

Hingga saat ini, Bank Muamalat Indonesia telah memiliki 338 jaringan kantor layanan seluruh Indonesia. Inovasi yang dilakukan Bank Muamalat Indonesia terus berlanjut. Layanan Muamalat Mobile, peluncuran produk Zafirah Proteksi Sejahtera, kerjasama dengan klub sepak bola dunia Arsenal FC, merupakan bagian dari wujud transformasi Bank Muamalat Indonesia menjadi Bank yang Islami, Modern dan Profesional.

Saat ini, Bank Muamalat Indonesia dimiliki oleh pemegang saham Islamic Development Bank atau IDB (32,7%), Boubyan Bank, Kuwait (22,0%), Atwill Holdings Limited, Saudi Arabia (17,9%), National Bank of Kuwait (8,5%), dan beberapa badan usaha dan individu lainnya. Boubyan Bank dimiliki oleh National Bank of Kuwait.

sumber: [https://news.treni.co.id/umat-islam-mendukung-muamalat-dan-industri-syariah/](https://news.treni.co.id/umat-islam-mendukung-muamalat-dan-industri-syariah/ "https://news.treni.co.id/umat-islam-mendukung-muamalat-dan-industri-syariah/")