+++
categories = ["inspirasi"]
date = "2018-03-30T10:45:51+07:00"
description = "Gubernur Bank Indonesia Agus DW Martowardojo meraih penghargaan sebagai Tokoh Ekonomi Syariah dalam ajang Anugerah Syariah Republika 2017"
featured = []
thumbnail = "/images/agus-2-1024x576.jpeg"
slug = ""
tags = ["syariah"]
title = "Tokoh Ekonomi Syariah Indonesia yang Visioner dan Komitmen"

+++
Di ajang Anugerah Syariah Republika (ASR) 2017 ini, Gubernur Bank Indonesia Agus DW Martowardojo meraih penghargaan sebagai Tokoh Ekonomi Syariah dalam ajang Anugerah Syariah Republika (ASR) 2017. 

Biografi Agus bisa ditelusuri dari dunia kerjanya yang tak lepas dari studi kuliahnya. Pria lulusan UI ini memulai karier sebagai pegawai hingga direktur bank. Puncaknya, ia menjadi menteri keuangan dan gubernur Bank Indonesia. 

Agus Dermawan Wintarto Martowardojo biasa dipanggil Agus lahir di Amsterdam, Belanda, 24 Januari1956. Meski lahir di luar negeri, Agus menyelesaikan masa sekolahnya di Jakarta.

Agus Martowardojo sekolah di SD Budi Waluyo, SMP Pangudi Luhur, dan SMA Pangudi luhur Jakarta. Setelah itu, ia melanjutkan ke Fakultas Ekonomi, Universitas Indonesia (UI). Ia berhasil meraih gelar sarjana ekonomi pada usia 28 tahun. 

Dalam pendidikan ini, Agus juga meneruskan kuliahnya bidang perbankan di State University of New York dan Stanford University di Amerika, lantas melanjutkan ke Institute Banking & Finance di Singapura.

Agus memulai kariernya pada usia 28 tahun di perbankan sebagai Officer Development Program (ODP) di Bank of America, sebagai International Loan Office. Kariernya terus menanjak, pada akhirnya Agus berkerja di Bank Niaga sebagai Vice President, Corporate Banking Group di Surabaya dan Jakarta.

Pada tahun 1994, ia menjadi Deputy Chief Executive Officer Maharani Holding. Setelah itu ia kembali ke dunia bank menjabat sebagai Direktur Utama Bank Bumiputera. 

Ia berpindah lagi, kali ini ke bank plat merah. Dia didaulat sebagai Direktur Bank Mandiri pada usia 54 tahun dan menjabat dari tahun 2005 hingga tahun 2010.

Setelah tidak menjabat sebagai direktur utama Bank Mandiri, pada tahun 2010, ia diangkat menjadi menteri keuangan menggantikan Sri Mulyani Indrawati. Pada Maret Tahun 2013, Agus Dermawan Wintarto Martowardojo terpilih menjadi Gubernur Bank Indonesia untuk periode 2013-2018.

[![](https://news.treni.co.id/wp-content/uploads/2017/12/agus-2-1024x576.jpeg =1024x576)](https://news.treni.co.id/wp-content/uploads/2017/12/agus-2.jpeg)

Dalam sambutannya ketika menerima Penghargaan sebagai Tokoh Ekonomi Syariah yang diberikan oleh Boy Thohir ini, Agus menjelaskan, pengembangan ekonomi dan keuangan syariah di Indonesia sudah lama dilaksanakan. 

Dalam amandeman UU Perbankan pada 1998 sudah terdapat komitmen untuk menumbuhkembangkan ekonomi syariah. Komitmen juga terlihat dalam UU No 21 Tahun 2008 yang mengesahkan UU Perbankan Syariah. Kemudian UU Nomor 19 Tahun 2008 mengesahkan UU Surat Berharga Syariah Negara.

Dan ini adalah bentuk komitmen untuk pengembangan ekonomi syariah di Indonesia ditindaklanjuti seluruh pegiat ekonomi syariah sehingga kita bisa berkembang seperti sekarang ini. 

Bank Indonesia, menurut Agus, telah memiliki roadmap perbankan syariah yang salah satunya dimotori oleh Muliaman D Hadad yang saat ini menjabat sebagai Ketua Masyarakat Ekonomi Syariah (MES). 

Penghargaan ASR 2017 yang diberikan Republika memberikan kepada pelaku ekonomi syariah dinilai sebagai satu hal yang membanggakan untuk bisa membawa ekonomi syariah seperti sekarang ini.

Indonesia sebagai negara muslim terbesar di dunia memiliki ruang pengembangan ekonomi syariah yang maish sangat luas. Indonesia dikenal sebagai negara pengimpor produk halal nomor empat terbesar di dunia. 

Hal itu menjadi potensi ekonomi yang harus diambil oleh bangsa sendiri. Agus berharap agar Indonesia bisa memenuhi kebutuhan pasar untuk masyarakatnya sendiri, serta berkomitmen untuk mengekspor produk-produk halal ke luar negeri. “Indonesia masih terus menjadi pangsa pasar bagi produk-produk halal seperti makanan, kosmetik, obat-obatan, tourism dan fashion. 

Mari kita bersama berniat mengembangkan ekonomi syariah ke depan,” ungkap Agus dalam sambutannya yang diberi aplaus oleh para hadirin.

Bank Indonesia merasa terpanggil bersama lembaga-lembaga terkait seperti Kementerian Keuangan serta Kementerian Agama berniat mengembangkan ekonomi syariah lebih luas dan lebih besar ke depan.

Pemerintah telah membentuk Komite Naisonal Keuangan Syariah (KNKS) dan dipimpin oleh Presiden Joko Widodo. Hal itu menunjukkan komitmen kementerian dan lembaga dalam memajukan ekonomi syariah. 

Dalam tiga tahun terakhir, Bank Indonesia sudah membangun Departemen Ekonomi dan Keuangan Syariah (DEKS). Agus melihat inisiatif-inisiatif yang dijalankan DEKS sejalan dengan KNKS.

DEKS mengembangkan tiga pilar utama antara lain pendalaman pasar keuangan syariah, pengembangan ekonomi syariah termasuk di dalamnya mengembangkan halal value chain dan ekonomi pondok pesantren, serta assesment risiko dan edukasi ekonomi syariah. “Dengan ketiga pilar itu diharapkan ke depan kita bisa melihat perkembangan ekonomi syariah lebih baik,” tambah Agus lagi.

Melalui sinergi kementerian/lembaga dan industri syariah, perkembangan ekonomi keuangan syariah akan lebih cepat dengan menjalankan dua pilar. 

Keduanya yakni sektor keuangan komersial syariah dan sektor keuangan sosial syariah. “Keduanya akan menjadi dasar untuk kita bisa mengembangkan ekonomi syariah lebih baik, lebih terukur dan ekonomi yang akan menjadi pendamping yang baik dari pengembangan ekonomi konvensional,” ucapnya.

Di akhir sambutannya, Agus mengajak kepada semua kalangan untuk berkomitmen dalam mengembangkan ekonomi syariah ke dewan. Sehingga akan membawa Indonesia menjadi maju, makmur dan berkeadilan.

sumber: [https://news.treni.co.id/tokoh-ekonomi-syariah-indonesia-yang-visioner-dan-komitmen/](https://news.treni.co.id/tokoh-ekonomi-syariah-indonesia-yang-visioner-dan-komitmen/ "https://news.treni.co.id/tokoh-ekonomi-syariah-indonesia-yang-visioner-dan-komitmen/")

  